const Hapi = require('@hapi/hapi');
const Wreck = require('@hapi/wreck');
const { join, resolve } = require('path');
// const { readFileSync } = require('fs');
// const Http2 = require('http2');

const baseUrl = 'https://api.weather.gov';
const options = {
    headers: {
        'Content-Type': 'application/json',
        accept: 'application/ld+json',
        'App-Version': '1.0',
        'User-Agent': 'quarantined-weather-app-thanks-covid-19'
    }
}

// const serverOptions = {
//     key: readFileSync(join(__dirname, 'localhost-privkey.pem')),
//     cert: readFileSync(join(__dirname, 'localhost-cert.pem'))
// };

// create http2 secure server listener
// const listener = Http2.createSecureServer(serverOptions);

/**
 * These next few things are here just to respect a free API and not hammer it with requests
 * during development and performance testing.
 *
 * Basically we cache everything here but throw in some pretend latency.
 */
const cache = new Map();

const mockLatency = () => {
    return new Promise(r => {
        setTimeout(() => {
            r();
        }, 0)
    });
};

const toWeatherDate = (date) => {
    const parts = date.toISOString().split('.');
    return encodeURIComponent(`${parts.slice(0, parts.length - 1)}Z`);
};

const filterByDate = (observations) => {
    const usedDates = [];
    observations['@graph'] = observations['@graph'].filter(o => {
       const date = o.timestamp.split('T')[0];
       const used = usedDates.includes(date);

       if (!used) {
           usedDates.push(date)
       }

       return !used;
    });
    return observations;
}

const getObservations = async (id, startDate, endDate, count) => {
    const start = toWeatherDate(startDate);
    const end = toWeatherDate(endDate);
    const key = `${id}-${count}`;

    const cached = cache.get(key);

    if (cached) {
        await mockLatency();
        return cached;
    }

    try {
        const { payload } = await Wreck.get(`${baseUrl}/stations/${id}/observations?start=${start}&end=${end}&limit=200`, options);
        const observations = filterByDate(JSON.parse(payload.toString()));
        cache.set(key, observations);
        return observations
    } catch(e) {
        return e;
    }
};

/**
 * Semi-realism starts here
 */
const initServer = async (path, port = 3000) => {
    const assetPath = resolve(__dirname, '../', 'dist', path);
    const server = Hapi.server({
        // listener,
        port: port,
        host: 'localhost'
    });

    await server.register([
        {
            plugin: require('@hapi/inert')
        }
    ]);

    server.route({
        method: 'POST',
        path: '/api/login',
        handler: async (request) => {
            await mockLatency();
            return request.payload;
        }
    });

    server.route({
        method: 'GET',
        path: '/api/states',
        handler: async () => {
            await mockLatency();
            return require('./states.json');
        }
    });

    server.route({
        method: 'GET',
        path: '/api/stations',
        handler: async (request) => {
            const params = request.query;
            const key = `stations-${params.state}`;
            const cached = cache.get(key);
            if (cached) {
                await mockLatency();
                return cached;
            }

            try {
                const { payload } = await Wreck.get(`${baseUrl}/stations?state=${params.state}`, options);
                const stations = payload.toString();
                cache.set(key, stations);
                return stations;
            } catch (e) {
                console.error(e);
                return e;
            }
        }
    });

    server.route({
        method: 'GET',
        path: '/api/temperature',
        handler: async (request) => {
            const params = request.query;
            const end = new Date();
            const start = new Date(end.getFullYear(), end.getMonth(), end.getDate() - params.count);
            const data = await getObservations(params.station, start, end, params.count);

            return JSON.stringify(data['@graph'].map(f => ({
                timestamp: f.timestamp,
                temperature: f.temperature.value
            })));
        }
    });

    server.route({
        method: 'GET',
        path: '/api/wind',
        handler: async (request) => {
            const params = request.query;
            const end = new Date();
            const start = new Date(end.getFullYear(), end.getMonth(), end.getDate() - 3);
            const data = await getObservations(params.station, start, end, params.count);

            return JSON.stringify(data['@graph'].map(f => ({
                direction: f.windDirection.value,
                speed: f.windSpeed.value,
                gust: f.windGust.value
            })));
        }
    });

    // SPA shenanigans
    // Static Assets
    server.route({
        method: 'GET',
        path: `/${path}/{param*}`,
        handler: {
            directory: {
                path: [assetPath],
                listing: false,
                index: ['index.html']
            }
        }
    });

    server.route({
        method: 'GET',
        path: `/{param*}`,
        handler: (r, h) => {
            return h.file(join(assetPath, 'index.html'))
        }
    });

    // return index.html for everything else
    server.ext('onPostHandler', (request, h) => {
        const response = request.response;
        if (response.isBoom) {
            if (response.output.statusCode === 404) {
                return h.file(join(assetPath, 'index.html'));
            } else {
                console.error(response);
            }
        }

        return response;
    });

    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});

module.exports = initServer;
