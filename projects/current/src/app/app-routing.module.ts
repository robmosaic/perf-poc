import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './services/authGuard';
import { LoginAuthGuard } from './services/loginAuthGuard';

import { DefaultLayoutComponent } from './containers/default-layout';
import { LoginComponent } from './views/login/login.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
    },
    {
        path: 'login',
        component: LoginComponent,
        canActivate: [LoginAuthGuard],
        pathMatch: 'full',
        data: {
            title: 'Login Page'
        }
    },
    {
        path: '',
        component: DefaultLayoutComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'Home'
        },
        children: [
            {
                path: 'dashboard',
                data: {
                    title: 'Dashboard'
                },
                loadChildren: () =>
                    import('./views/dashboard/dashboard.module').then(m => m.DashboardModule)
            },
            {
                path: 'states',
                data: {
                    title: 'States'
                },
                loadChildren: () =>
                    import('./views/states/states.module').then(m => m.StatesModule)
            }
        ]
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
