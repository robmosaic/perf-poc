import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { EMPTY, Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ErrorInterceptorService implements HttpInterceptor {
    constructor(private router: Router, private toastr: ToastrService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            catchError(err => {
                if (err.status === 401) {
                    // Authentication service cannot be used because it is not always defined.
                    localStorage.removeItem('currentUser');
                    this.router.navigate(['/login']);
                    this.toastr.warning(
                        'Your session has expired. Please log in again.',
                        'Session Expired'
                    );
                    return EMPTY;
                }
                return throwError(err);
            })
        );
    }
}
