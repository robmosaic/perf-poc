import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class LoginAuthGuard implements CanActivate {
    constructor(private _authService: AuthenticationService, private _router: Router) {}

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        if (this._authService.isUserLoggedIn()) {
            console.log('Already logged in - navigating to home');
            // Logged in
            // Added in APP-833 as temporary workaround to mitigate issue where index.html gets
            // out of date with backend index.html
            window.location.replace(`${window.location.origin}/dashboard`);
            // TODO: Add back in with APP-887
            // this._router.navigate(['/dashboard']);
            return false;
        }

        return true;
    }
}
