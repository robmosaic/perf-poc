import { ErrorHandler, Injectable, Injector } from '@angular/core';

/**
 * Handles any errors that occur in the application
 */
@Injectable({
    providedIn: 'root'
})
export class MosaicErrorHandler extends ErrorHandler implements ErrorHandler {

    constructor(private injector: Injector) {
        super();
    }

    public handleError(error: any): void {
        super.handleError(error);
    }

    /**
     * Reloads the current web page
     */
    public reloadPage(): void {
        window.location.reload();
    }
}
