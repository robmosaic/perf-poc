import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';

import { environment } from '../../environments/environment';

import { User } from '../models/User';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUser: User;
    private _userBehavior: BehaviorSubject<User> = new BehaviorSubject<User>(undefined);

    get user(): User {
        return this.currentUser;
    }

    set user(user: User) {
        this.currentUser = user;
        this._userBehavior.next(this.currentUser);
        if (user) {
            localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
        } else {
            localStorage.clear();
        }
    }

    constructor(private http: HttpClient) {
        const user = localStorage.getItem('currentUser');
        if (user) {
            this.user = JSON.parse(user);
        } else if (!environment.authEnabled) {
            this.user = new User('rob.desisto@gmail.com');
        }
    }

    isUserLoggedIn() {
        return !!this.currentUser && !!localStorage.getItem('currentUser');
    }

    getUserBehavior(): Observable<User> {
        return this._userBehavior.asObservable();
    }

    getUserState() {
        return this.currentUser ? this.currentUser.state : '';
    }

    setUserState(state: string) {
        this.user = Object.assign({}, this.currentUser, { state });
    }

    // Mock logging in
    login(email: string): Observable<User> {
        return this.http.post<User>('/api/login', { email });
    }
}
