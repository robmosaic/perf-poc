import { fakeAsync, tick } from '@angular/core/testing';
import { asyncData } from '../../testing/async-observable-helpers';
import { MosaicErrorHandler } from './mosaic-error-handler.service';

describe('MosaicErrorHandler', () => {
    let errorHandler: MosaicErrorHandler;

    let injectorMock: { get: jasmine.Spy };

    beforeEach(() => {
        injectorMock = jasmine.createSpyObj('injectorMock', ['get']);
        errorHandler = new MosaicErrorHandler(injectorMock);
    });

    describe('handleError', () => {
        it('generic error should just log error', () => {
            spyOn(console, 'error');
            spyOn(errorHandler, 'reloadPage');

            const error = {
                message: 'Some error occurred'
            };
            errorHandler.handleError(error);
            expect(console.error).toHaveBeenCalled();
            expect(injectorMock.get).not.toHaveBeenCalled();
            expect(errorHandler.reloadPage).not.toHaveBeenCalled();
        });

        it('should reload the page if a numeric (prod) chunk error occurred', fakeAsync(() => {
            spyOn(console, 'error');
            spyOn(errorHandler, 'reloadPage');
            const dialogMock = jasmine.createSpyObj('dialogMock', ['open']);
            injectorMock.get.and.returnValue(dialogMock);
            const afterClosed = () => {
                return asyncData({});
            };
            dialogMock.open.and.returnValue({
                afterClosed: afterClosed
            });

            const error = {
                message: 'Loading chunk 12 failed'
            };

            errorHandler.handleError(error);
            tick();
            expect(console.error).toHaveBeenCalled();
            expect(errorHandler.reloadPage).toHaveBeenCalled();
        }));

        it('should reload the page if a string (dev) chunk error occurred', fakeAsync(() => {
            spyOn(console, 'error');
            spyOn(errorHandler, 'reloadPage');
            const dialogMock = jasmine.createSpyObj('dialogMock', ['open']);
            injectorMock.get.and.returnValue(dialogMock);
            const afterClosed = () => {
                return asyncData({});
            };
            dialogMock.open.and.returnValue({
                afterClosed: afterClosed
            });

            const error = {
                message: 'Loading chunk some-test-chunk failed'
            };

            errorHandler.handleError(error);
            tick();
            expect(console.error).toHaveBeenCalled();
            expect(errorHandler.reloadPage).toHaveBeenCalled();
        }));
    });
});
