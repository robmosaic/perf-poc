import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private _authService: AuthenticationService, private _router: Router) {}

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        if (!this._authService.isUserLoggedIn()) {
            console.log('No user logged in - navigating to login');
            // Logged in
            this._router.navigate(['/login']);
            return false;
        }

        return true;
    }
}
