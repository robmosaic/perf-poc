import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { StationPayload } from '../models/station-payload';
import { AuthenticationService } from './authentication.service';
import { ChartPoint } from '../models/chart-point';
import { WindRow } from '../models/wind-row';

@Injectable({ providedIn: 'root' })
export class WeatherService {
    constructor(private http: HttpClient, private auth: AuthenticationService) {}

    getStations(): Observable<StationPayload> {
        const state = this.auth.getUserState();
        return this.http.get<StationPayload>(`/api/stations?state=${state}`);
    }

    getTemps(id: string, count: number): Observable<ChartPoint[]> {
        return this.http.get<ChartPoint[]>(`/api/temperature?station=${toId(id)}&count=${count}`);
    }

    getWind(id: string): Observable<WindRow[]> {
        return this.http.get<WindRow[]>(`/api/wind?station=${toId(id)}`);
    }
}

const toId = (id: string): string => {
    const parts =  id.split('/');
    return parts[parts.length - 1];
};
