import { Component } from '@angular/core';

@Component({
    selector: 'msc-default-layout',
    templateUrl: './default-layout.component.html',
    styleUrls: ['./default-layout.component.scss']
})
export class DefaultLayoutComponent {
    opened = true;
}
