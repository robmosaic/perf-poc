import { Component, OnInit } from '@angular/core';
import { ColDef, GridReadyEvent, GridSizeChangedEvent } from 'ag-grid-community';

import { StatesService } from '../../services/states.service';
import { State } from '../../models/state';

@Component({
    selector: 'msc-states',
    templateUrl: 'states.component.html',
    styleUrls: ['states.component.scss']
})
export class StatesComponent implements OnInit {
    public columns: ColDef[];
    public states: State[];

    constructor(private statesService: StatesService) {}

    ngOnInit() {
        this.columns = [
            {
                field: 'name',
                headerName: 'Name'
            },
            {
                field: 'value',
                headerName: 'Abbreviation'
            }
        ];

        this.statesService.getStates().subscribe({
            next: (data) => {
                // AG Grid DOES use onpush :)
                this.states = Object.keys(data).map(value => {
                    return {
                        value,
                        name: data[value]
                    };
                });
            }
        });
    }

    sizeGrid({ api }: GridReadyEvent | GridSizeChangedEvent) {
        api.sizeColumnsToFit();
    }
}

