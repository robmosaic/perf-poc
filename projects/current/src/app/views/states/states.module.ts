import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgGridModule } from 'ag-grid-angular';

import { MscCommonModule } from '../../common/common.module';

import { StatesRoutingModule } from './states.routing.module';
import { StatesComponent } from './states.component';

@NgModule({
    imports: [
        CommonModule,
        AgGridModule,
        StatesRoutingModule,
        MscCommonModule
    ],
    declarations: [
        StatesComponent
    ]
})
export class StatesModule {}
