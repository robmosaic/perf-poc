import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from '../../services/authentication.service';
import { User } from '../../models/User';

@Component({
    selector: 'msc-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent {
    model = {
        email: ''
    };

    constructor(private auth: AuthenticationService, private router: Router) {}

    login() {
        this.auth.login(this.model.email).subscribe({
            next: ({ email }) => {
                this.auth.user = new User(email);
                this.router.navigateByUrl('/dashboard');
            },
            error: (e) => {
                console.error('Well this is pretty embarrassing seeing as it is mocked.');
                throw e;
            }
        });
    }
}
