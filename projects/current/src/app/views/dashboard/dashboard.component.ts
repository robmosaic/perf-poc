import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

import { AuthenticationService } from '../../services/authentication.service';
import { WeatherService } from '../../services/weather.service';
import { Station } from '../../models/station';

@Component({
    selector: 'msc-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
    stations: Station[];

    private userSub: Subscription;

    constructor(private auth: AuthenticationService, private weather: WeatherService, private toastr: ToastrService) {}

    ngOnInit() {
        this.userSub = this.auth.getUserBehavior().subscribe({
            next: () => {
                this.loadStations();
            }
        });
    }

    private loadStations() {
        this.weather.getStations().subscribe({
            next: ({ '@graph': graph}) => {
                this.stations = graph.sort((a, b) => a.name.localeCompare(b.name));
            },
            error: (e) => {
                this.toastr.error('Could not load stations');
                console.error(e);
                throw e;
            }
        });
    }

    ngOnDestroy() {
        this.userSub.unsubscribe();
    }
}
