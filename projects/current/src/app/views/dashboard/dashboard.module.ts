import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { NgSelectModule } from '@ng-select/ng-select';
import { AgGridModule } from 'ag-grid-angular';

import { MscCommonModule } from '../../common/common.module';

import { DashboardRoutingModule } from './dashboard.routing.module';
import { DashboardComponent } from './dashboard.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        MatCardModule,
        MatIconModule,
        NgSelectModule,
        AgGridModule,
        DashboardRoutingModule,
        MscCommonModule
    ],
    declarations: [
        DashboardComponent
    ]
})
export class DashboardModule {}
