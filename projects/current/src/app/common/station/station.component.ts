import { Component, Input, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

import { WeatherService } from '../../services/weather.service';
import { Station } from '../../models/station';
import { DisplayType } from '../../models/display-type';
import { ChartPoint } from '../../models/chart-point';
import { WindRow } from '../../models/wind-row';

@Component({
    selector: 'msc-station',
    templateUrl: './station.component.html',
    styleUrls: ['./station.component.scss'],
})
export class StationComponent implements OnInit {
    @Input() station: Station;

    displayType: DisplayType = DisplayType.Recent;
    tempData: ChartPoint[];
    windData: WindRow[];

    constructor(private weather: WeatherService, private toastr: ToastrService) {}

    ngOnInit() {
        this.loadTempData();
    }

    setDisplay(displayType: string) {
        this.displayType = displayType as DisplayType;

        if (displayType === DisplayType.Wind) {
            this.tempData = undefined;
            this.loadWindData();
        } else {
            this.windData = undefined;
            this.loadTempData();
        }
    }

    private loadTempData() {
        this.weather.getTemps(this.station['@id'], this.displayType === DisplayType.Week ? 7 : 3).subscribe({
           next: (data: ChartPoint[]) => {
               this.tempData = data;
           },
            error: (e) => {
                this.toastr.error(`Could not load temperature for ${this.station.name}`);
                console.error(e);
                throw e;
            }
        });
    }

    private loadWindData() {
        this.weather.getWind(this.station['@id']).subscribe({
            next: (data) => {
                this.windData = data;
            },
            error: (e) => {
                this.toastr.error(`Could not load wind data for ${this.station.name}`);
                console.error(e);
                throw e;
            }
        });
    }
}
