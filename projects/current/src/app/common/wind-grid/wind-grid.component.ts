import { Component, Input, OnInit } from '@angular/core';
import { ColDef, GridReadyEvent, GridSizeChangedEvent } from 'ag-grid-community';

import { WindRow } from '../../models/wind-row';

@Component({
    selector: 'msc-wind-grid',
    templateUrl: './wind-grid.component.html',
    styleUrls: ['./wind-grid.component.html']
})
export class WindGridComponent implements OnInit {
    @Input() data: WindRow[];

    public columns: ColDef[];

    ngOnInit() {
        this.columns = [
            {
                field: 'direction',
                headerName: 'Direction'
            },
            {
                field: 'speed',
                headerName: 'Speed'
            },
            {
                field: 'gust',
                headerName: 'Gust'
            }
        ];
    }

    sizeGrid({ api }: GridReadyEvent | GridSizeChangedEvent) {
        api.sizeColumnsToFit();
    }
}
