// Taken with appreciation from https://loading.io/css/
import { Component } from '@angular/core';

@Component({
    selector: 'msc-spinner',
    template: '<div class="lds-dual-ring"></div>',
    styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent {}
