import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { AgGridModule } from 'ag-grid-angular';

import { ChartComponent } from './chart/chart.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { StateMenuComponent } from './state-menu/state-menu.component';
import { StationComponent } from './station/station.component';
import { WindGridComponent } from './wind-grid/wind-grid.component';

@NgModule({
    imports: [
        AgGridModule,
        CommonModule,
        FormsModule,
        MatButtonModule,
        MatCardModule,
        MatFormFieldModule,
        MatMenuModule,
        MatSelectModule,
    ],
    declarations: [
        ChartComponent,
        SpinnerComponent,
        StateMenuComponent,
        StationComponent,
        WindGridComponent
    ],
    exports: [
        SpinnerComponent,
        StateMenuComponent,
        StationComponent
    ]
})
export class MscCommonModule {}
