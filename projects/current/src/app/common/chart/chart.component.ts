import { AfterViewInit, Component, ElementRef, Input, NgZone, OnChanges, OnDestroy, SimpleChanges, ViewChild } from '@angular/core';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';

import { ChartPoint } from '../../models/chart-point';

am4core.useTheme(am4themes_animated);

@Component({
    selector: 'msc-chart',
    template: '<div #container style="width: 100%; height: 500px"></div>',
    styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements AfterViewInit, OnChanges, OnDestroy {
    @Input() data: ChartPoint[];
    @ViewChild('container') container: ElementRef;

    private chart: am4charts.XYChart;

    constructor(private zone: NgZone) {}

    ngOnChanges({ data }: SimpleChanges) {
        if (this.chart) {
            this.ngOnDestroy();
            this.ngAfterViewInit();
        }
    }

    ngAfterViewInit() {
        this.zone.runOutsideAngular(() => {
            const chart = am4core.create(this.container.nativeElement, am4charts.XYChart);
            chart.data = this.data;
            const categoryAxis = chart.xAxes.push(new am4charts.DateAxis());
            categoryAxis.dataFields.date = 'timestamp';
            const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.title.text = 'Temp (C)';
            const series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueY = 'temperature';
            series.dataFields.dateX = 'timestamp';
            this.chart = chart;
        });
    }

    ngOnDestroy() {
        this.zone.runOutsideAngular(() => {
            if (this.chart) {
                this.chart.dispose();
            }
        });
    }
}
