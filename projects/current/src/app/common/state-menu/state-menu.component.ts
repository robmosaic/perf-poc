import { Component, OnInit } from '@angular/core';
import { MatSelectChange } from '@angular/material/select';

import { StatesService } from '../../services/states.service';
import { AuthenticationService } from '../../services/authentication.service';
import { State } from '../../models/state';

@Component({
    selector: 'msc-state-menu',
    templateUrl: './state-menu.component.html',
    styleUrls: ['./state-menu.component.scss']
})
export class StateMenuComponent implements OnInit {
    public state: string;
    public states: State[] = [];

    constructor(private auth: AuthenticationService, private statesService: StatesService) {}

    /**
     * This is horrendous, but I've actually seen this kind of thing in the code,
     * so it's a good place to murder change detection
     */
    ngOnInit() {
        this.state = this.auth.getUserState();
        this.statesService.getStates().subscribe({
            next: (data) => {
              Object.keys(data).forEach((k) => {
                  this.states.push({
                      value: k,
                      name: data[k]
                  });
              });
            }
        });
    }

    stateChange({ value }: MatSelectChange) {
        this.auth.setUserState(value);
    }
}
