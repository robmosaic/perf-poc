export enum DisplayType {
    Recent = 'recent',
    Week = 'week',
    Wind = 'wind'
}
