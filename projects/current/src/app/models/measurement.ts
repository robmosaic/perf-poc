export interface Measurement {
    value: number;
    unitCode: string;
}
