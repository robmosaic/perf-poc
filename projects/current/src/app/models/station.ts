import { EntityType } from './entity-type';
import { Measurement } from './measurement';

export interface Station {
    '@id': string;
    '@type': EntityType;
    geometry: string;
    elevation: Measurement;
    stationIdentifier: string;
    name: string;
    timezone: string;
}
