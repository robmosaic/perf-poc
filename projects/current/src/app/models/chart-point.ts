export interface ChartPoint {
    timestamp: string;
    temperature: number;
}
