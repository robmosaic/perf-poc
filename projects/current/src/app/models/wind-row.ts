export interface WindRow {
    direction: number;
    speed: number;
    gust: number;
}
