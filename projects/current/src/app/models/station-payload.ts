import { Station } from './station';

export interface StationPayload {
    '@context': { [key: string]: any };
    '@graph': Station[];
    observationStations: string[];
}
