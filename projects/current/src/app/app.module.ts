import { NgModule, ErrorHandler } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { CovalentBreadcrumbsModule } from '@covalent/core/breadcrumbs';
import { GridsterModule } from 'angular-gridster2';
import { ToastrModule } from 'ngx-toastr';

import { MscCommonModule } from './common/common.module';

import { AuthGuard } from './services/authGuard';
import { LoginAuthGuard } from './services/loginAuthGuard';
import { ErrorInterceptorService } from './services/error-interceptor.service';
import { MosaicErrorHandler } from './services/mosaic-error-handler.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { DefaultLayoutComponent } from './containers/default-layout';
import { LoginComponent } from './views/login/login.component';

@NgModule({
    declarations: [
        AppComponent,
        DefaultLayoutComponent,
        LoginComponent
    ],
    imports: [
        HttpClientModule,
        CovalentBreadcrumbsModule,
        FormsModule,
        ReactiveFormsModule,
        GridsterModule,
        MatButtonModule,
        MatCardModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        MatMenuModule,
        MatSelectModule,
        MatSidenavModule,
        MatToolbarModule,
        MatTooltipModule,
        CKEditorModule,
        ToastrModule.forRoot({
            preventDuplicates: true
        }),
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        MscCommonModule
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ErrorInterceptorService,
            multi: true
        },
        {
            provide: ErrorHandler,
            useClass: MosaicErrorHandler
        },
        AuthGuard,
        LoginAuthGuard
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
