import { ChangeDetectionStrategy, Component, Inject, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';

import { SmartComponent } from '@core/core.types';
import { LayoutComponent } from '../layout/layout.component';

import { StationsLoad } from '@dashboard/state/dashboard.actions';
import { DashboardState } from '@dashboard/state/dashboard.state';
import { Station } from '@dashboard/./dashboard.types';

@Component({
    selector: 'msn-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardComponent implements OnInit, SmartComponent {
    @Select(DashboardState.stations) stations$: Observable<Station[]>;

    constructor(private store: Store, @Inject(LayoutComponent) private layout: LayoutComponent) {}

    ngOnInit() {
        this.layout.register(this);
    }

    load() {
        this.store.dispatch(new StationsLoad());
    }
}
