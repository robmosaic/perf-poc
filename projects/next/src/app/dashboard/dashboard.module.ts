import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';

import { ChartsModule } from '@charts/public';
import { GridModule } from '@grid/public';
import { SharedModule } from '@shared/public';

import { DashboardRoutingModule } from '@dashboard/dashboard.routing.module';
import { DashboardState } from '@dashboard/state/dashboard.state';
import { DashboardComponent } from '@dashboard/dashboard.component';
import { StationComponent } from '@dashboard/station/station.component';
import { TempGridComponent } from '@dashboard/station/temp-grid/temp-grid.component';
import { WindGridComponent } from '@dashboard/station/wind-grid/wind-grid.component';

@NgModule({
    imports: [
        ChartsModule,
        GridModule,
        SharedModule,
        DashboardRoutingModule,
        NgxsModule.forFeature([DashboardState])
    ],
    declarations: [
        DashboardComponent,
        StationComponent,
        TempGridComponent,
        WindGridComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DashboardModule {}
