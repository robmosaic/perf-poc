/* Legacy */
export * from '@legacy/models/display-type';

export const toId = (id: string): string => {
    const parts =  id.split('/');
    return parts[parts.length - 1];
};
