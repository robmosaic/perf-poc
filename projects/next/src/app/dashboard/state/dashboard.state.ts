import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { ChartPoint } from '@charts/public';
import { WeatherService} from '@dashboard/services/weather.service';
import { StationsLoad, StationTemp, StationWeek, StationWind } from '@dashboard/state/dashboard.actions';
import { Station, StationPayload, WindRow } from '@dashboard/dashboard.types';

export class DashboardStateModel {
    stations: Station[];
    recent: { [id: string]: ChartPoint };
    week: { [id: string]: ChartPoint };
    wind: { [id: string]: WindRow };
}

@State<DashboardStateModel>({
    name: 'dashboard',
    defaults: {
        stations: undefined,
        recent: {},
        week: {},
        wind: {}
    }
})
@Injectable()
export class DashboardState {
    @Selector()
    static stations(state: DashboardStateModel): Station[] {
        return state.stations;
    }

    constructor(private weather: WeatherService) {}

    @Action(StationsLoad)
    stationsLoad({ patchState }: StateContext<DashboardStateModel>): Observable<StationPayload> {
        return this.weather.getStations().pipe(tap(({ '@graph': graph}) => {
            const stations = graph.sort((a, b) => a.name.localeCompare(b.name));
            patchState({ stations });
        }));
    }

    @Action(StationTemp)
    stationTemp({ patchState, getState }: StateContext<DashboardStateModel>, { id, count }: StationTemp): Observable<ChartPoint[]> {
        return this.weather.getTemps(id, count).pipe(tap((data) => {
            const temps = getState().recent;
            patchState({ recent: Object.assign({}, temps, { [id]: data }) });
        }));
    }

    @Action(StationWeek)
    stationWeek({ patchState, getState }: StateContext<DashboardStateModel>, { id, count }: StationWeek): Observable<ChartPoint[]> {
        return this.weather.getTemps(id, count).pipe(tap((data) => {
            const temps = getState().week;
            patchState({ week: Object.assign({}, temps, { [id]: data }) });
        }));
    }

    @Action(StationWind)
    stationWind({ patchState, getState }: StateContext<DashboardStateModel>, { id }: StationWind): Observable<WindRow[]> {
        return this.weather.getWind(id).pipe(tap((data) => {
            const winds = getState().wind;
            patchState({ wind: Object.assign({}, winds, { [id]: data }) });
        }));
    }
}
