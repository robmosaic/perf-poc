abstract class StationObservation {
    protected constructor(public id: string) {}
}

export class StationsLoad {
    static readonly type = '[dashboard] StationsLoad';
}

export class StationTemp extends StationObservation {
    static readonly type = '[dashboard] StationTemp';

    constructor(id, public count = 3) {
        super(id);
    }
}

export class StationWeek extends StationObservation {
    static readonly type = '[dashboard] StationWeek';

    constructor(id: string, public count = 7) {
        super(id);
    }
}

export class StationWind extends StationObservation {
    static readonly type = '[dashboard] StationWind';

    constructor(id: string) {
        super(id);
    }
}
