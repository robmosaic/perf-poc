import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { ColDef } from 'ag-grid-community';

import { TempRow } from '@dashboard/dashboard.types';

@Component({
    selector: 'msn-temp-grid',
    templateUrl: './temp-grid.component.html',
    styleUrls: ['./temp-grid.component.html'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TempGridComponent implements OnInit {
    @Input() data: TempRow[];

    public columns: ColDef[];

    ngOnInit() {
        this.columns = [
            {
                field: 'timestamp',
                headerName: 'Time'
            },
            {
                field: 'temperature',
                headerName: 'Temp'
            }
        ];
    }
}
