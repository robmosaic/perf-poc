import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { ColDef } from 'ag-grid-community';

import { WindRow } from '@dashboard/dashboard.types';

@Component({
    selector: 'msn-wind-grid',
    templateUrl: './wind-grid.component.html',
    styleUrls: ['./wind-grid.component.html'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class WindGridComponent implements OnInit {
    @Input() data: WindRow[];

    public columns: ColDef[];

    ngOnInit() {
        this.columns = [
            {
                field: 'direction',
                headerName: 'Direction'
            },
            {
                field: 'speed',
                headerName: 'Speed'
            },
            {
                field: 'gust',
                headerName: 'Gust'
            }
        ];
    }
}
