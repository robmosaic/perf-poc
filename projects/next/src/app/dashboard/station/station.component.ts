import { ChangeDetectionStrategy, Component, Input, OnChanges } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';

import { ChartPoint } from '@charts/public';
import { Station, WindRow } from '@dashboard/dashboard.types';
import { StationTemp, StationWeek, StationWind } from '@dashboard/state/dashboard.actions';
import { DisplayType, toId } from '@dashboard/dashboard.constants';

@Component({
    selector: 'msn-station',
    templateUrl: './station.component.html',
    styleUrls: ['./station.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class StationComponent implements OnChanges {
    @Input() station: Station;

    displayType: DisplayType = DisplayType.Recent;
    recent$: Observable<ChartPoint[]>;
    week$: Observable<ChartPoint[]>;
    wind$: Observable<WindRow[]>;
    recentAsTable = false;

    constructor(private store: Store) {
        this.recent$ = this.store.select(state => state.dashboard.recent[toId(this.station['@id'])]);
        this.week$ = this.store.select(state => state.dashboard.week[toId(this.station['@id'])]);
        this.wind$ = this.store.select(state => state.dashboard.wind[toId(this.station['@id'])]);
    }

    ngOnChanges() {
        this.load();
    }

    onDisplay(displayType: string) {
        this.displayType = displayType as DisplayType;
        this.load();
    }

    private load() {
        switch (this.displayType) {
            case DisplayType.Week:
                this.store.dispatch(new StationWeek(toId(this.station['@id'])));
                break;
            case DisplayType.Wind:
                this.store.dispatch(new StationWind(toId(this.station['@id'])));
                break;
            default:
                this.store.dispatch(new StationTemp(toId(this.station['@id'])));
                break;
        }
    }
}
