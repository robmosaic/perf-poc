import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ChartPoint } from '@charts/public';
import { StationPayload, WindRow } from '../dashboard.types';

@Injectable({ providedIn: 'root' })
export class WeatherService {
    constructor(private http: HttpClient) {}

    getStations(): Observable<StationPayload> {
        return this.http.get<StationPayload>(`/api/stations`);
    }

    getTemps(id: string, count: number): Observable<ChartPoint[]> {
        return this.http.get<ChartPoint[]>(`/api/temperature?station=${id}&count=${count}`);
    }

    getWind(id: string): Observable<WindRow[]> {
        return this.http.get<WindRow[]>(`/api/wind?station=${id}`);
    }
}
