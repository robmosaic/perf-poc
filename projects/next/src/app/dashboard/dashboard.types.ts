/* Legacy */
export * from '@legacy/models/station';
export * from '@legacy/models/station-payload';
export * from '@legacy/models/wind-row';

export interface TempRow {
    timestamp: string;
    temperature: number;
}
