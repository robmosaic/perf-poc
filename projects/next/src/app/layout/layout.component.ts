import { AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Select } from '@ngxs/store';

import { CoreState, SmartComponent, User } from '@core/public';

@Component({
    selector: 'msn-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LayoutComponent implements AfterViewInit, OnDestroy {
    @Select(CoreState.user) user$: Observable<User>;
    showNav = true;

    private container: SmartComponent;
    private sub: Subscription;

    register(comp: SmartComponent) {
        this.container = comp;
    }

    ngAfterViewInit(): void {
        this.user$.subscribe({
            next: () => {
                if (this.container) {
                    this.container.load();
                }
            }
        });
    }

    ngOnDestroy(): void {
        if (this.sub) {
            this.sub.unsubscribe();
        }
    }
}
