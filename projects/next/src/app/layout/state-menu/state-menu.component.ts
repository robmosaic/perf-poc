import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';

import { CoreState, StatesLoad, User, UserUpdate, USState } from '@core/public';

@Component({
    selector: 'msn-state-menu',
    templateUrl: './state-menu.component.html',
    styleUrls: ['./state-menu.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class StateMenuComponent implements OnInit {
    @Select(CoreState.states) states$: Observable<USState[]>;
    @Select(CoreState.user) user$: Observable<User>;

    constructor(private store: Store) {}

    ngOnInit() {
        this.store.dispatch(new StatesLoad());
    }

    onSelect({ detail }: CustomEvent) {
        this.store.dispatch(new UserUpdate({ state: detail.value }));
    }
}
