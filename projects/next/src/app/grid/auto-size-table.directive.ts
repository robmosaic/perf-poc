import { Directive, ElementRef, HostBinding, HostListener, Input } from '@angular/core';
import { GridReadyEvent, GridSizeChangedEvent } from 'ag-grid-community';

/**
 * A simple directive to cut down on the duplicated logic/bindings.
 *
 * It does three things:
 * * Sets with width style attribute to 100%
 * * adds the ag-theme-material class in a non-destructive manner
 * * listens to resize events and calls the correct resize method based on the number of columns
 *
 * Usage:
 * <ag-grid-angular mscAutoSizeTable></mscAutoSizeTable>
 *
 * Forcing Size to Fit:
 * [mscAutoSizeTable]="true" will force the directive to call "sizeColumnsToFit"
 * instead of "autoSizeAllColumns".
 *
 * All the static stuff could be made dynamic as needed.
 */
@Directive({ selector: '[msnAutoSizeTable]' })
export class AutoSizeTableDirective {
    // TODO: refactor the table service to allow getting these
    static COLUMNS_FIT_VIEW_THRESHOLD = 7;
    static COLUMNS_FIT_WIDE_VIEW_THRESHOLD = 15;
    static WIDE_VIEW_THRESHOLD = 1657;

    /**
     * Set the theme without stomping on other classes
     */
    @HostBinding('class.ag-theme-material') isMaterialTheme = true;

    /**
     * Set the width, we can make this configurable
     */
    @HostBinding('style.width') width = '100%';

    /**
     * Use opacity to hide the grid until we've sized the columns
     */
    @HostBinding('style.opacity')
    get opacity() {
        return this.isReady ? 1 : 0;
    }

    /**
     * Set to true to force this directive to use size to fit
     */
    @Input() mscAutoSizeTable = false;

    private isReady = false;

    /**
     * If the behavior of this directive ever seems off, start here.
     */
    private static toFitThreshold(colCount: number, width: number): boolean {
        return (
            colCount <= AutoSizeTableDirective.COLUMNS_FIT_VIEW_THRESHOLD ||
            (colCount <= AutoSizeTableDirective.COLUMNS_FIT_WIDE_VIEW_THRESHOLD &&
                width <= AutoSizeTableDirective.WIDE_VIEW_THRESHOLD)
        );
    }

    constructor(private element: ElementRef) {}

    /**
     * Initial sizing the flip the opacity switch
     */
    @HostListener('gridReady', ['$event'])
    @HostListener('gridColumnsChanged', ['$event'])
    onReady({ api, columnApi }: GridReadyEvent) {
        this.sizeToFit({
            api,
            columnApi,
            clientWidth: this.element.nativeElement.clientWidth
        } as GridSizeChangedEvent);
        this.isReady = true;
    }

    @HostListener('gridSizeChanged', ['$event'])
    sizeToFit({ api, columnApi, clientWidth }: GridSizeChangedEvent) {
        if (
            this.mscAutoSizeTable ||
            AutoSizeTableDirective.toFitThreshold(
                columnApi.getAllDisplayedColumns().length,
                clientWidth
            )
        ) {
            api.sizeColumnsToFit();
        } else {
            columnApi.autoSizeAllColumns();
        }
    }
}
