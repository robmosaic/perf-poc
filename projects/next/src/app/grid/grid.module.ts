import { NgModule } from '@angular/core';
import { AgGridModule } from 'ag-grid-angular';

import { SharedModule } from '@shared/shared.module';

import { AutoSizeTableDirective } from '@grid/auto-size-table.directive';

@NgModule({
    declarations: [
        AutoSizeTableDirective
    ],
    exports: [
        AgGridModule,
        AutoSizeTableDirective
    ],
    imports: [
        AgGridModule,
        SharedModule
    ]
})
export class GridModule {}
