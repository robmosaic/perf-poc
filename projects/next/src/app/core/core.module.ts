import { CUSTOM_ELEMENTS_SCHEMA, ErrorHandler, NgModule, Optional, SkipSelf } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgxsModule } from '@ngxs/store';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { ToastrModule } from 'ngx-toastr';

import { environment } from '../../environments/environment';

import { SpinnerComponent } from '@legacy/common/spinner/spinner.component';

import { ErrorInterceptorService } from '@legacy/services/error-interceptor.service';
import { MosaicErrorHandler } from '@legacy/services/mosaic-error-handler.service';

import { SharedModule } from '@shared/public';

import { CoreState } from '@core/state/core.state';
import { LoadingInterceptor } from '@core/services/loading.interceptor';
import { StateInterceptor } from '@core/services/state.interceptor';

import { LoadingSpinnerComponent } from '@core/loading-spinner/loading-spinner.component';
import { LoginComponent } from '@core/login/login.component';

@NgModule({
    declarations: [
        LoadingSpinnerComponent,
        LoginComponent,
        SpinnerComponent
    ],
    exports: [
        HttpClientModule,
        LoadingSpinnerComponent,
        LoginComponent
    ],
    imports: [
        HttpClientModule,
        NgxsModule.forRoot([CoreState], {
            developmentMode: !environment.production
        }),
        NgxsStoragePluginModule.forRoot({
            key: 'core'
        }),
        NgxsReduxDevtoolsPluginModule.forRoot({
            disabled: environment.production
        }),
        NgxsLoggerPluginModule.forRoot({
            disabled: environment.production
        }),
        SharedModule,
        ToastrModule.forRoot({
            preventDuplicates: true
        }),
    ],
    providers: [
        {
            provide: ErrorHandler,
            useClass: MosaicErrorHandler
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ErrorInterceptorService,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: StateInterceptor,
            multi: true
        },
        { provide: HTTP_INTERCEPTORS, useClass: LoadingInterceptor, multi: true }
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CoreModule {
    constructor( @Optional() @SkipSelf() parentModule: CoreModule) {
        if (parentModule) {
            throw new Error('CoreModule has already been loaded. You should only import Core modules in the AppModule only.');
        }
    }
}
