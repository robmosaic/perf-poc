import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Subject } from 'rxjs';

import { LoadingService } from '@core/services/loading.service';

@Component({
    selector: 'msn-loading-spinner',
    template: `<msc-spinner *ngIf="isLoading | async"></msc-spinner>`,
    styleUrls: ['./loading-spinner.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoadingSpinnerComponent {
    isLoading: Subject<boolean> = this.loaderService.isLoading;

    constructor(private loaderService: LoadingService) {}
}
