import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { User } from '@core/core.constants';

@Injectable({ providedIn: 'root' })
export class AuthService {
    constructor(private http: HttpClient) {}

    // Mock logging in
    login(email: string): Observable<User> {
        return this.http.post<User>('/api/login', { email });
    }
}
