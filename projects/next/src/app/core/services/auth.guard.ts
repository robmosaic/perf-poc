import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Store } from '@ngxs/store';

import { User } from '@core/core.constants';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor(private router: Router, private store: Store) {}

    canActivate(): boolean {
        const user = this.store.selectSnapshot<User>((state) => state.core.user);

        if (!user) {
            this.router.navigate(['/login']);
        }

        return !!user;
    }
}
