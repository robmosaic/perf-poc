import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Store } from '@ngxs/store';

import { User } from '@core/core.constants';

@Injectable({ providedIn: 'root' })
export class LoginGuard implements CanActivate {
    constructor(private router: Router, private store: Store) {}

    canActivate(): boolean {
        const user = this.store.selectSnapshot<User>((state) => state.core.user);

        if (user) {
            console.log('Already logged in - navigating to home');
            // Logged in
            // Added in APP-833 as temporary workaround to mitigate issue where index.html gets
            // out of date with backend index.html
            window.location.replace(`${window.location.origin}/dashboard`);
            // TODO: Add back in with APP-887
            // this._router.navigate(['/dashboard']);
        }

        return !user;
    }
}
