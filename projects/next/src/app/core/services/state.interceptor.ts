import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';

@Injectable()
export class StateInterceptor implements HttpInterceptor {
    private static cloneRequest(request: HttpRequest<any>, id: string) {
        return request.clone({
           params : request.params.set('state', id),
        });
    }

    constructor(private store: Store) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const stateId = this.store.selectSnapshot<string>((state) => state.core.user?.state);
        return next.handle(stateId ? StateInterceptor.cloneRequest(req, stateId) : req);
    }
}
