/* Legacy */
import { User } from '@legacy/models/User';
export * from '@legacy/models/User';
export * from '@legacy/services/states.service';

export const DEFAULT_STATE = {
    user: undefined
};

export const PERF_TEST_STATE = {
    user: new User('rob@mosaic.text')
};
