import { State as USState } from '@legacy/models/state';
export { USState };

export interface SmartComponent {
    load(): void;
}
