import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';

import { UserLogin } from '@core/state/core.actions';

@Component({
    selector: 'msn-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent {
    model = {
        email: ''
    };

    constructor(private router: Router, private store: Store) {}

    login() {
        this.store.dispatch(new UserLogin(this.model.email)).subscribe({
            next: () => {
                this.router.navigateByUrl('/dashboard');
            },
            error: (e) => {
                console.error('Well this is pretty embarrassing seeing as it is mocked.');
                throw e;
            }
        });
    }
}
