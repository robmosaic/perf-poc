import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { environment } from '../../../environments/environment';

import { AuthService } from '@core/services/auth.service';
import { DEFAULT_STATE, PERF_TEST_STATE, StatesService, User } from '@core/core.constants';
import { StatesLoad, UserLogin, UserLogout, UserUpdate } from '@core/state/core.actions';
import { USState } from '@core/core.types';

export class CoreStateModel {
    user: User;
    states: USState[];
}

// This is wonky just because of the hack in place for perf testing
const baseState = {
  states: undefined,
};
const defaults = Object.assign(baseState, environment.authEnabled ? DEFAULT_STATE : PERF_TEST_STATE);

@State<CoreStateModel>({
    name: 'core',
    defaults
})
@Injectable()
export class CoreState {
    @Selector()
    static states(state: CoreStateModel): USState[] {
        return state.states;
    }

    @Selector()
    static user(state: CoreStateModel): User {
        return state.user;
    }

    constructor(private auth: AuthService, private states: StatesService) {}

    @Action(StatesLoad)
    statesLoad({ patchState }: StateContext<CoreStateModel>): Observable<USState[]> {
        return this.states.getStates().pipe(tap((stateObj) => {
            const states: USState[] = Object.keys(stateObj).map(a => ({
                value: a,
                name: stateObj[a]
            }));
            patchState({ states });
        })) as Observable<USState[]>;
    }

    @Action(UserLogin)
    login({ patchState }: StateContext<CoreStateModel>, { username }: UserLogin): Observable<User> {
        return this.auth.login(username).pipe(tap(({ email }) => {
            patchState({ user: new User(email) });
        })) as Observable<User>;
    }

    @Action(UserLogout)
    logout({ patchState }: StateContext<CoreStateModel>) {
        patchState({ user: undefined });
    }

    @Action(UserUpdate)
    userUpdate({ patchState, getState }: StateContext<CoreStateModel>, { update }: UserUpdate) {
        const user = getState().user;
        patchState({ user: Object.assign({}, user, update) });
    }
}
