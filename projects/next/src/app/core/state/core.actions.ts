import { User } from '@core/core.constants';

export class StatesLoad {
    static readonly type = '[core] StatesLoad';
}

export class UserLogin {
    static readonly type = '[core] UserLogin';

    constructor(public username: string) {}
}

export class UserLogout {
    static readonly type = '[core] UserLogout';
}

export class UserUpdate {
    static readonly type = '[core] UserUpdate';

    constructor(public update: Partial<User>) {}
}
