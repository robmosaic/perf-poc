import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'msn-root',
    template: '<router-outlet></router-outlet><msn-loading-spinner></msn-loading-spinner>',
    styleUrls: ['./app.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
    title = 'next';
}
