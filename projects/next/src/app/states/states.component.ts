import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ColDef } from 'ag-grid-community';
import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';

import { CoreState, USState } from '@core/public';

@Component({
    selector: 'msn-states',
    templateUrl: 'states.component.html',
    styleUrls: ['states.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class StatesComponent {
    @Select(CoreState.states) states$: Observable<USState[]>;

    public columns: ColDef[] = [
        {
            field: 'name',
            headerName: 'Name'
        },
        {
            field: 'value',
            headerName: 'Abbreviation'
        }
    ];

    constructor() {}
}

