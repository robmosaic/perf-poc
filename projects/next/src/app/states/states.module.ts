import { NgModule } from '@angular/core';

import { GridModule } from '@grid/public';
import { SharedModule } from '@shared/public';

import { StatesRoutingModule } from './states.routing.module';
import { StatesComponent } from './states.component';

@NgModule({
    imports: [
        GridModule,
        SharedModule,
        StatesRoutingModule
    ],
    declarations: [
        StatesComponent
    ]
})
export class StatesModule {}
