import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MosaicCoreModule } from '@mosaic/angular-core';

@NgModule({
    declarations: [],
    exports: [
        CommonModule,
        FormsModule,
        MosaicCoreModule,
        ReactiveFormsModule
    ],
    imports: [
        CommonModule,
        FormsModule,
        MosaicCoreModule,
        ReactiveFormsModule
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SharedModule {}
