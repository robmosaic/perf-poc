import {
    AfterViewInit,
    ChangeDetectionStrategy,
    Component,
    ElementRef,
    Input,
    NgZone,
    OnChanges,
    OnDestroy,
    SimpleChanges,
    ViewChild
} from '@angular/core';

import { ChartPoint } from '@charts/charts.types';
import { ChartService } from '@charts/chart.service';

@Component({
    selector: 'msn-chart',
    template: '<div #container style="width: 100%; height: 500px"></div>',
    styleUrls: ['./chart.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChartComponent implements AfterViewInit, OnChanges, OnDestroy {
    @Input() data: ChartPoint[];
    @ViewChild('container') container: ElementRef;

    private chart: any;

    constructor(private zone: NgZone, private charts: ChartService) {}

    ngOnChanges({ data }: SimpleChanges) {
        if (this.chart) {
            this.ngOnDestroy();
            this.ngAfterViewInit();
        }
    }

    async ngAfterViewInit() {
        const [am4core, am4charts] = await this.charts.getAmCharts();
        this.zone.runOutsideAngular(() => {
            const chart = am4core.create(this.container.nativeElement, am4charts.XYChart);
            chart.data = this.data;
            const categoryAxis = chart.xAxes.push(new am4charts.DateAxis());
            categoryAxis.dataFields.date = 'timestamp';
            const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.title.text = 'Temp (C)';
            const series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueY = 'temperature';
            series.dataFields.dateX = 'timestamp';
            this.chart = chart;
        });
    }

    ngOnDestroy() {
        this.zone.runOutsideAngular(() => {
            if (this.chart) {
                this.chart.dispose();
            }
        });
    }
}
