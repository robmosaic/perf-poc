import { NgModule } from '@angular/core';

import { SharedModule } from '@shared/shared.module';

import { ChartComponent } from '@charts/chart/chart.component';
import { ChartService } from '@charts/chart.service';

@NgModule({
    declarations: [
        ChartComponent
    ],
    exports: [
        ChartComponent
    ],
    imports: [SharedModule],
    providers: [
        ChartService
    ]
})
export class ChartsModule {}
