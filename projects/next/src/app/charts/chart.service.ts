import { Injectable } from '@angular/core';

@Injectable()
export class ChartService {
    async getAmCharts() {
        try {
            const [core, chart, theme] = await Promise.all([
                import('@amcharts/amcharts4/core'),
                import('@amcharts/amcharts4/charts'),
                import('@amcharts/amcharts4/themes/material')
            ]);

            core.useTheme(theme.default);
            core.options.queue = true;
            core.options.onlyShowOnViewport = true;
            return [core as any, chart as any];
        } catch (e) {
            throw e;
        }
    }
}
