import { NgModule } from '@angular/core';
import { PreloadAllModules, Routes, RouterModule } from '@angular/router';

import { AuthGuard, LoginGuard, LoginComponent } from '@core/public';

import { LayoutComponent } from './layout/layout.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
    },
    {
        path: 'login',
        component: LoginComponent,
        canActivate: [LoginGuard],
        pathMatch: 'full',
        data: {
            title: 'Login Page'
        }
    },
    {
        path: '',
        component: LayoutComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'Home'
        },
        children: [
            {
                path: 'dashboard',
                data: {
                    title: 'Dashboard'
                },
                loadChildren: () =>
                    import('./dashboard/dashboard.module').then(m => m.DashboardModule)
            },
            {
                path: 'states',
                data: {
                    title: 'States'
                },
                loadChildren: () =>
                    import('./states/states.module').then(m => m.StatesModule)
            }
        ]
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
