# PerfPoc

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.6.

NOTE: all the below are just notes I scrambled. I will make sense of them later.

## Dependencies
| Dependency | Size   | GZipped | Est. Time |
| ---------- |-------:| -------:|----------:|
| ag-grd (c) | 736.6  | 148.6   | 2.97s     |
| ag-grd (e) | 705.6  | 145.9   | 2.92s     |
| amcharts   | 919.1  | 197.5   | 4.28s     |
| @cke/bal   | 598.8  | 153.0   | 3.06s     |
| @cov/high  | 767.7  | 246.9   | 4.95s     |
| goo/phone  | 533.1  | 108.4   | 2.17s     |
| moment.js  | 231.7  | 65.9    | 1.32s     |

** Sizes ate in kb
*** Time is for 3G so on WIFI, ethernet it will be faster
**** Obviously these don't happen linearly

## Build Findings For Actual App
### Resources To Render
#### Current Build
```
ng build --prod --aot --buildOptimizer --commonChunk --vendorChunk --optimization --progress
```

| Bundle  | Size     | GZipped   |
| ------- |---------:| ---------:|
| vendor  | 4.29mb   | 1.05mb    |
| main    | 3.2mb    | 415.9kb   |
| polyfi  | 56.2     | 19.0kb    |
| styles  | 214kb    | 37.0kb    |
| ------- | -------- | --------- |
| *Total* | *7760kb* | *1522kb*  |

#### CLI Tweaks Build
```
ng build --prod --aot --buildOptimizer --commonChunk=false --vendorChunk=false --optimization --progress
```

| Bundle  | Size     | GZipped   |
| ------- |---------:| ---------:|
| main    | 7.5mb    | 1.5b      |
| polyfil | 56.2     | 19.0kb    |
| styles  | 214kb    | 37.0kb    |
| ------- | -------- | --------- |
| *Total* | *7760kb* | *1522kb*  |

#### Opportunities
| Dependency | Size     | GZipped   |
| ---------- |---------:| ---------:|
| @ckeditor  | 582.2kb  | 145.2kb   |
| amcharts   | 919.1kb  | 197.5kb   |
| ag-grd (e) | 705.6kb  | 145.9kb   |
| ag-grd (c) | 736.6kb  | 148.6kb   |
| html2canva | 161.3kb  | 38.6kb    |
| moment.js  | 231.7kb  | 65.9kb    |
| material   | 253.5kb  | 47.5kb    |
| ---------- | -------- | --------- |
| *Total*    | *3596kb* | *643.2kb* |

### Deepdive On the Src Hotspots
| Name    | Size     | GZipped   |
| ------- |---------:| ---------:|
| common  | 1.28mb   | 153.2kb   |
| pref.sr | 55.8kb   | 16.5kb    |
| writeba | 797kb    | 86.1kb    |
| ------- | -------- | --------- |
| *Total* | *2133kb* | *255.8kb* |


## I have questions
```typescript
{
    path: 'salesforce-objects',
    canActivate: [AdminGuard],
    component: SalesforceObjectsComponent,
    loadChildren: () =>
        import('./views/salesforce-objects/salesforce-objects.module').then(
            m => m.SalesforceObjectsModule
        )
},
```

## Some things I did
Broke common up into discreet features
State management
basically all of this  https://www.amcharts.com/docs/v4/concepts/performance/
